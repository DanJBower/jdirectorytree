package tree.model;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

/*
 * Class that automatically generates the tree
 * so that it contains all directories accessible drives.
 * 
 * Does not access hidden directories or directories
 * that require administrator privileges.
 */

public class ComputerDirectoryTreeModel implements TreeModel {
	private DefaultMutableTreeNode root = new DefaultMutableTreeNode();
	
	public ComputerDirectoryTreeModel() {
		createRootLayout();
	}
	
	private void createRootLayout() {
		for(File path : File.listRoots()) { //Loops through the "root" directories on a computer - Determined by system
			if(path.exists()) {
				if(addable(path)) {
					if(hasDirectory(path)) {
						root.add(new DefaultMutableTreeNode(path));
					}
				}
			}
		}
	}
	
	//Checks whether the drive can be added
	//Only adds local drives or USBs
	private boolean addable(File path) {
		FileSystemView fsv = FileSystemView.getFileSystemView();
		String type = fsv.getSystemTypeDescription(path);
		//System.out.println(type); //For Debugging Purposes
		if(type != null) {
			if(type.equalsIgnoreCase("Local Disk")) {
				return true;
			} else if(type.equalsIgnoreCase("USB Drive")) {
				return true;
			}
		}
		return false;
	}
	
	//Not used
	@Override
    public void addTreeModelListener(javax.swing.event.TreeModelListener l) {}

	//Overrides the original getChild so that the method returns the correct directory
    @Override
    public Object getChild(Object parent, int index) {
		if(parent != root && parent instanceof DefaultMutableTreeNode) {
			File f = (File) (((DefaultMutableTreeNode) parent).getUserObject());
			return listDirectories(f)[index]; //TODO Can we do this once and then store the result?
		} else if(parent != root) {
			File f = (File) parent;
			return listDirectories(f)[index];
		}
		return root.getChildAt(index);
    }

	//Overrides the original getChildCount so that
	//the method returns the correct value
    @Override
    public int getChildCount(Object parent) {
		if(parent != root && parent instanceof DefaultMutableTreeNode) {
			File f = (File) (((DefaultMutableTreeNode) parent).getUserObject());
			if (!f.isDirectory()) {
				return 0;
			} else {
				return listDirectories(f).length;
			}
		} else if(parent != root) {
			File f = (File) parent;
			if (!f.isDirectory()) {
				return 0;
			} else {
				return listDirectories(f).length;
			}
		}
		return root.getChildCount();
    }
    
	//Overrides the original hasChildren so that
	//the method returns the correct value
    public boolean hasChildren(Object parent) {
    	if(parent != root && parent instanceof DefaultMutableTreeNode) {
			return hasDirectory((File) (((DefaultMutableTreeNode) parent).getUserObject()));
		} else if(parent != root) {
			return hasDirectory((File) parent);
		}
		return root.getChildCount() != 0 ? true : false;
    }
    
    //Overrides the original getIndexOfChild so that
  	//the method returns the correct value
    @Override
    public int getIndexOfChild(Object parent, Object child) {
		if(parent != root && parent instanceof DefaultMutableTreeNode) {
			File par = (File) (((DefaultMutableTreeNode) parent).getUserObject());
			File ch = (File) child;
			return Arrays.asList(listDirectories(par)).indexOf(ch);
		} else if(parent != root) {
			File par = (File) parent;
			File ch = (File) child;
			return Arrays.asList(listDirectories(par)).indexOf(ch);
		}
		
		return root.getIndex((TreeNode) child);
    }

    @Override
    public Object getRoot() {
        return root;
    }
    
    //There should technically be no leaves as every directory
    //could potentially have a sub directory in it.
    @Override
    public boolean isLeaf(Object node) {
		return false;
    }

    //Not used
    @Override
    public void removeTreeModelListener(javax.swing.event.TreeModelListener l) {}
    
    //Not used
    @Override
    public void valueForPathChanged(javax.swing.tree.TreePath path, Object newValue) {}
	
    //Lists all the sub directories of the given directory
    //if it is accessible. This will not work for folders
    //that require administrator privileges to view.
	private File[] listDirectories(File path) {
		File[] files = DataStore.DATA_STORE.get(path);
		
		if(files == null) {
			List<File> directories = new ArrayList<File>();
			for(File temp : path.listFiles()) {
				Path dir = Paths.get(temp.getAbsolutePath());
				DirectoryStream<Path> stream;
				try {
					stream = Files.newDirectoryStream(dir);
					stream.close();
					directories.add(temp);
				} catch (IOException e) {
					//System.out.println("Not a accessible dir");
				}
			}

			files = directories.toArray(new File[0]);
			DataStore.DATA_STORE.put(path, files);
			return files;
		}
		
		return files;
	}
	
	//Written to improve performance of listDirectories(path) != 0
	private boolean hasDirectory(File path) {
		//System.out.println("Testing: " + path.getAbsolutePath());
		Path dir = Paths.get(path.getAbsolutePath());
		DirectoryStream<Path> stream;
		try {
			stream = Files.newDirectoryStream(dir);
			for(Path p : stream) {
				//System.out.println("Testing for Children: " + p.toString());
				DirectoryStream<Path> tempStream;
				try {
					tempStream = Files.newDirectoryStream(p);
					tempStream.close();
					return true;
				} catch (IOException e) {
					//System.out.println("Not a accessible dir");
				}
			}
			stream.close();
			return false;
		} catch (IOException e) {
			//System.out.println("Not a accessible dir");
			return false;
		}
	}
}