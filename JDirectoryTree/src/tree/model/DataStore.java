package tree.model;

import java.io.File;
import java.util.TreeMap;

import tree.utility.AlphanumComparator;

public class DataStore {
	public static TreeMap<File, File[]> DATA_STORE = new TreeMap<File, File[]>(new AlphanumComparator());
}