package tree.handlers;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTree;
import javax.swing.SwingUtilities;

import tree.design.TreeHandleUI;

/*
 * Class which controls expand / collapse handle animations
 * and other mouse related functions
 */

public class MouseHandler extends MouseAdapter {
	private static final int FADE_TIME_IN_MILLISECONDS = 300;
	private JTree tree = null;
	private TreeHandleUI tUI = null;
	
	public MouseHandler(JTree tree) {
		this.tree = tree;
		tUI = (TreeHandleUI) tree.getUI();
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
		tUI.fadeHandles(true, FADE_TIME_IN_MILLISECONDS); //Fades handles in
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
		tUI.fadeHandles(false, FADE_TIME_IN_MILLISECONDS); //Fades handles out
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		//Clears selection when mouse is pressed outside of on of the nodes
		if((SwingUtilities.isLeftMouseButton(e) || SwingUtilities.isRightMouseButton(e))
				&& tree.getRowForLocation(e.getX(), e.getY()) == -1)
			tree.clearSelection();
	}
}