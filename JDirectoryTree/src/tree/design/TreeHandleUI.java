package tree.design;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.TreePath;

import tree.model.ComputerDirectoryTreeModel;
import tree.utility.AlphaImageIcon;

/*
 * This mainly handles the look and feel of the JTree
 * It effects areas such as the expand controls of nodes,
 * the way the nodes look, and how lines are drawn on the
 * tree if they are drawn at all
 */

public class TreeHandleUI extends BasicTreeUI {
	///Variables
	private JTree tree = null;
	private boolean lines = false;
	private boolean lineTypeDashed = true;
	private Icon rolloverIcon = null;
	private boolean iconRolloverEnabled = false;
	private UpdateHandler uH = null;
	private ComputerDirectoryTreeModel tM; //Used so that we can quickly check if the file contains any children
	private boolean isLeftToRight(Component c) {
		return c.getComponentOrientation().isLeftToRight();
	}
	
	private Timer timer;
	private float alphaVal = 0;
	private AlphaImageIcon collapsedAlphaIcon = new AlphaImageIcon(new ImageIcon("resources/closed.png"), alphaVal);
	private AlphaImageIcon expandedAlphaIcon = new AlphaImageIcon(new ImageIcon("resources/open.png"), alphaVal);
	private AlphaImageIcon rolloverAlphaIcon = new AlphaImageIcon(new ImageIcon("resources/rollover.png"), alphaVal);
	private float percentComplete = 0;
	
	///Constructors
	//Sets most of the icons for the JTree and changes the default mouse handler
	public TreeHandleUI(JTree tree) {
		this.tree = tree;
		uH = new UpdateHandler(this.tree);
		this.tree.addMouseMotionListener(uH);
		
		EventQueue.invokeLater(() -> tM = (ComputerDirectoryTreeModel) treeModel); //Is like this so that tM does not return an NPE
		EventQueue.invokeLater(() -> setCollapsedIcon(collapsedAlphaIcon));
		EventQueue.invokeLater(() -> setExpandedIcon(expandedAlphaIcon));
		EventQueue.invokeLater(() -> setRolloverIcon(rolloverAlphaIcon));
		
		removeLines(true); //Removes all the visible lines on the JTree
	}
	
	///Methods
	//Methods used for rollover icon
	public void setRolloverIcon(Icon rolloverG) {
		Icon oldValue = rolloverIcon;
		rolloverIcon = rolloverG;
		setIconRolloverEnabled(true);
		if (rolloverG != oldValue) {
			tree.repaint();
		}
	}
	
	private void setIconRolloverEnabled(boolean handleRolloverEnabled) {
		boolean oldValue = iconRolloverEnabled;
		iconRolloverEnabled = handleRolloverEnabled;
		if (handleRolloverEnabled != oldValue) {
			tree.repaint();
		}
	}
	
	public void fadeHandles(boolean show, int msDur) {
		if(timer != null)
			timer.stop();
		
		long startTime = System.currentTimeMillis();
		
		//This fixes an occasional issue where a mouse would exit the JTree and re-enter too fast
		if(percentComplete > 1) {
			percentComplete = 1;
		} else if(percentComplete < 0) {
			percentComplete = 0;
		}
		
		if(show) {
			msDur = Math.round((1 - percentComplete) * msDur);
		} else {
			msDur = Math.round(percentComplete * msDur);
		}
		
		final float dur = msDur; //Fix inner class issue
		
		timer = new Timer(40, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				long currentTime = System.currentTimeMillis();
				percentComplete = (currentTime - startTime) / dur;
				
				if(!show) {
					percentComplete = 1 - percentComplete;
				}
				
				if(percentComplete < 0 || percentComplete > 1) {
			    	timer.stop();
			    }
				
				collapsedAlphaIcon.setAlpha(percentComplete);
				expandedAlphaIcon.setAlpha(percentComplete);
				rolloverAlphaIcon.setAlpha(percentComplete);
				
				tree.repaint();
			}
		});
		
		timer.start();
	}
	
	//Paints the correct icon for the expand control
	//Ie when the mouse is over the collapsed icon it
	//changes to the rollover icon
	@Override
	protected void paintExpandControl(Graphics g,
									  Rectangle clipBounds, Insets insets,
									  Rectangle bounds, TreePath path,
									  int row, boolean isExpanded,
									  boolean hasBeenExpanded,
									  boolean isLeaf) {
		Object value = path.getLastPathComponent();
		
		if(tM.hasChildren(value)) {
			int middleXOfKnob;
			if(isLeftToRight(tree)) {
				middleXOfKnob = bounds.x - getRightChildIndent() + 1;
			} else {
				middleXOfKnob = bounds.x + bounds.width + getRightChildIndent() - 1;
			}
			
			int middleYOfKnob = bounds.y + (bounds.height / 2);

			if(isExpanded) {
				Icon expandedIcon = getExpandedIcon();
				if(expandedIcon != null) {
					  drawCentered(tree, g, expandedIcon, middleXOfKnob, middleYOfKnob);
				}
			} else if(isLocationInExpandControl(path, uH.getXPos(), uH.getYPos())
					& !isExpanded && iconRolloverEnabled) {
				if(row == uH.getRowHandle()) {
					if(rolloverIcon != null) {
						drawCentered(tree, g, rolloverIcon, middleXOfKnob, middleYOfKnob);
					}
				} else {
					Icon collapsedIcon = getCollapsedIcon();
					if(collapsedIcon != null) {
						drawCentered(tree, g, collapsedIcon, middleXOfKnob, middleYOfKnob);
					}
				}
			} else {
				Icon collapsedIcon = getCollapsedIcon();
				if(collapsedIcon != null) {
					drawCentered(tree, g, collapsedIcon, middleXOfKnob, middleYOfKnob);
				}
			}
		}
	}
	
	//Paints the row, updates background if mouse over row
	@Override
	protected void paintRow(Graphics g, Rectangle clipBounds,
							Insets insets, Rectangle bounds, TreePath path,
							int row, boolean isExpanded,
							boolean hasBeenExpanded, boolean isLeaf) {
		// Don't paint the renderer if editing this row.
		if(editingComponent != null && editingRow == row)
			return;

		int leadIndex;

		if(tree.hasFocus()) {
			leadIndex = getLeadSelectionRow();
		} else {
			leadIndex = -1;
		}
		
		Component component = ((FileNameRenderer) tree.getCellRenderer()).getTreeCellRendererComponent
					  (tree, path.getLastPathComponent(),
					   tree.isRowSelected(row), isExpanded, isLeaf, row,
					   (leadIndex == row), (row == uH.getRow()));

		rendererPane.paintComponent(g, component, tree, bounds.x, bounds.y,
									bounds.width, bounds.height, true);
	}
	
	//Small utility class used for retrieving information so that
	//tasks such as adding a rollover icon or rollover background
	//can be completed
	private class UpdateHandler extends BasicTreeUI.MouseHandler {
		private JTree t = null;
		private int xPos = 0;
		private int yPos = 0;
		
		public UpdateHandler(JTree tree) {
			t = tree;
		}
		
		@Override
		public void mouseMoved(MouseEvent e) {
			xPos = e.getX();
			yPos = e.getY();
			t.repaint();
		}
		
		public int getXPos() {
			return xPos;
		}
		
		public int getYPos() {
			return yPos;
		}
		
		public int getRow() {
			return t.getRowForLocation(xPos, yPos);
		}
		
		public int getRowHandle() {
			return getRowForPath(t, getClosestPathForLocation(t, xPos, yPos));
		}
	}
	
	//Methods used for remove lines
	public void removeLines(boolean ShowLines) {
		lines = ShowLines;
	}
	
	public void removeLines(boolean ShowLines, boolean dashedLines) {
		lines = ShowLines;
		lineTypeDashed = dashedLines;
	}
	
	@Override
	protected void paintDropLine(Graphics g) {
		if(!lines) {
			JTree.DropLocation loc = tree.getDropLocation();
			if (!isDropLine(loc)) {
				return;
			}

			Color c = UIManager.getColor("Tree.dropLineColor");
			if (c != null) {
				g.setColor(c);
				Rectangle rect = getDropLineRect(loc);
				g.fillRect(rect.x, rect.y, rect.width, rect.height);
			}
		}
	}
	
	@Override
	protected void paintHorizontalLine(Graphics g, JComponent c, int y, int left, int right) {
		if(!lines) {
			if (lineTypeDashed) {
				drawDashedHorizontalLine(g, y, left, right);
			} else {
				g.drawLine(left, y, right, y);
			}
		}
	}
	
	@Override
	protected void paintVerticalLine(Graphics g, JComponent c, int x, int top, int bottom) {
		if(!lines) {
			if (lineTypeDashed) {
				drawDashedVerticalLine(g, x, top, bottom);
			} else {
				g.drawLine(x, top, x, bottom);
			}
		}
	}
}