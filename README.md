`JDirectoryTree` is a extension to Java's `JTree`. It is designed to list a file structure from the root of a computer to every accessible folder. In other words, it will open the file as long as you don't require elevated user privileges.

This currently only works on windows but that can be changed relatively easy in `ComputerDirectoryTreeModel.java`. The section of code that handles what roots can be added is previewed below.

    private boolean addable(File path) {
		FileSystemView fsv = FileSystemView.getFileSystemView();
		String type = fsv.getSystemTypeDescription(path);
		//System.out.println(type); //For Debugging Purposes
		if(type != null) {
			if(type.equalsIgnoreCase("Local Disk"))
				return true;
			if(type.equalsIgnoreCase("USB Drive"))
				return true;
		}
		return false;
	}